/**
 * Max Control
 * Copyright (c) 2015 Jan-Michael Brummer
 *
 * This file is part of Max Control.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>

#include <cube_object.h>

GObject *cube_object = NULL;

/** cube_object signals array */
guint cube_object_signals[CCB_MAX] = { 0 };

/** Private cube_object data */
typedef struct _CubeObjectPrivate CubeObjectPrivate;

struct _CubeObjectPrivate {
  gchar dummy;
};

G_DEFINE_TYPE (CubeObject, cube_object, G_TYPE_OBJECT);

static void cube_object_create_signals(GObjectClass *g_object_class)
{
  cube_object_signals[CCB_NEW_DEVICE] = g_signal_new (
    "new-device",
    G_OBJECT_CLASS_TYPE (g_object_class),
    G_SIGNAL_RUN_FIRST,
    G_STRUCT_OFFSET (CubeObjectClass, new_device),
    NULL,
    NULL,
    g_cclosure_marshal_VOID__POINTER,
    G_TYPE_NONE,
    1,
    G_TYPE_POINTER);

  cube_object_signals[CCB_MODIFIED] = g_signal_new (
    "modified",
    G_OBJECT_CLASS_TYPE (g_object_class),
    G_SIGNAL_RUN_FIRST,
    G_STRUCT_OFFSET (CubeObjectClass, modified),
    NULL,
    NULL,
    g_cclosure_marshal_VOID__POINTER,
    G_TYPE_NONE,
    1,
    G_TYPE_POINTER);

  cube_object_signals[CCB_STATUS] = g_signal_new (
    "status",
    G_OBJECT_CLASS_TYPE (g_object_class),
    G_SIGNAL_RUN_FIRST,
    G_STRUCT_OFFSET (CubeObjectClass, status),
    NULL,
    NULL,
    g_cclosure_marshal_VOID__POINTER,
    G_TYPE_NONE,
    1,
    G_TYPE_POINTER);
}

static void cube_object_class_init(CubeObjectClass *klass)
{
  GObjectClass *g_object_class;

  g_object_class = G_OBJECT_CLASS (klass);

  /*g_type_class_add_private(klass, sizeof(CubeObjectPrivate)); */
  cube_object_create_signals (g_object_class);
}

static void cube_object_init(CubeObject *self)
{
}

GObject *cube_object_new(void)
{
  return g_object_new (CUBE_OBJECT_TYPE, NULL);
}
