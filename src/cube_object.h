/**
 * Max Control
 * Copyright (c) 2015 Jan-Michael Brummer
 *
 * This file is part of Max Control.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CUBE_OBJECT_H
#define CUBE_OBJECT_H

#include <cube.h>

typedef enum {
	CCB_NEW_DEVICE,
	CCB_MODIFIED,
	CCB_STATUS,
	CCB_MAX
} CubeCallbackId;

#define CUBE_OBJECT_TYPE (cube_object_get_type())

typedef struct _CubeObject CubeObject;
typedef struct _CubeObjectClass CubeObjectClass;

struct _CubeObject {
	GObject parent;
};

struct _CubeObjectClass {
	GObjectClass parent_class;
	void (*new_device)(struct cube_device *device);
	void (*modified)(struct cube *cube);
	void (*status)(struct cube *cube);
};

GObject *cube_object_new(void);

extern GObject *cube_object;
extern guint cube_object_signals[CCB_MAX];

#endif
